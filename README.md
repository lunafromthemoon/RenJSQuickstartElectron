# RenJS with Electron Quickstart

**Clone and run for a quick way to see RenJS as a desktop application.**

This is the RenJS Quickstart for Electron, based on the Electron Quickstart.

To run and package your game for the different platforms you'll need to install:

* node and npm
* electron-packager

Download and install the project

```bash
# Clone this repository
git clone git@gitlab.com:lunafromthemoon/RenJSQuickstartElectron.git
# Go into the repository
cd RenJSQuickstartElectron
# Install dependencies
npm install
```

Run the app locally

```bash
# Run it
npm start
```

Package it

```bash
# For linux
npm run package-linux
# For mac
npm run package-mac
# For windows
npm run package-win-64
npm run package-win-32

```